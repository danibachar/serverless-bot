/**
 * Created by danielbachar on 26/08/2017.
 */
const _            = require('lodash');
const wit        = require("./wit");
const _ = require('lodash');

const resHelper = {
    // _ : require('lodash');
    base_res:function() {
        return {
            "response": "", // what the bot will respond with
            "continue": true, // "true" will result in Motion AI continuing the flow based on connections, whie "false" will make Motion AI hit this module again when the user replies
            "customPayload": "", // OPTIONAL: working data to examine in future calls to this function to keep track of state
            "quickReplies": null, // OPTIONAL: a JSON string array containing suggested/quick replies to display to the user .. i.e., ["Hello","World"]
            "cards": null, // OPTIONAL: a cards JSON object to display a carousel to the user (see docs)
            "customVars": null, // OPTIONAL: an object or stringified object with key-value pairs to set custom variables eg: {"key":"value"} or '{"key":"value"}'
            //"nextModule": null //welcom module id 1018402 // OPTIONAL: the ID of a module to follow this Node JS module
        };
    },
    error_res:function() {
        return {
            "response": "Oops", // what the bot will respond with
            "continue": true, // "true" will result in Motion AI continuing the flow based on connections, whie "false" will make Motion AI hit this module again when the user replies
            "customPayload": "", // OPTIONAL: working data to examine in future calls to this function to keep track of state
            "quickReplies": null, // OPTIONAL: a JSON string array containing suggested/quick replies to display to the user .. i.e., ["Hello","World"]
            "cards": null, // OPTIONAL: a cards JSON object to display a carousel to the user (see docs)
            "customVars": null, // OPTIONAL: an object or stringified object with key-value pairs to set custom variables eg: {"key":"value"} or '{"key":"value"}'
            "nextModule": 1018402 //welcom module id 1018402 // OPTIONAL: the ID of a module to follow this Node JS module
        };
    },
    merge_res: function(json) {
        var base = this.base_res();
        _.assign(base, json);
        console.log('returning merged respons' ,base);
        return base;
    }
};

const searchHelper = {

    getSearchParamsByResponse: function (userRes, cb) {
        wit.recognize(userRes, (res) => {
            const sp = {};
            sp.ramRange = res.ramRange || { max:'128', min:'2' };
            sp.cpu = res.cpu || [];
            sp.screenSize = res.screenSize || [7,8,9,1,0,11,12,13,14,15,16,17,18,19];
            sp.priceRange = res.priceRange || { max:999999, min:1 };
            sp.warranty = res.warranty || 'true';
            if (cb) cb(sp, res.category);//res.category = one of supported categories ['laptop', 'tablet', 'phone']...etc
        });
    }

};

const replyHelper = {
    replyPrice: function (category) {
        switch (category) {
            case 'laptop':
                return ['0-1000', '1000-2000', '2000-3500', 'more then 3500'];
            case 'tablet':
                return ['0-500', '500-1000', '1000-1500', 'more then 1500'];
        }
    },
    replyScreenSize: function (category) {
        return ['small', 'medium', 'big'];
    },
    replyRefine: function () {
        return ['Refine?', 'New Search?'];
    },
    replyScreenSizeExample: function (category) {
        switch (category) {
            case 'laptop':
                return ['0-1000', '1000-2000', '2000-3500', 'more then 3500'];
            case 'tablet':
                return ['0-500', '500-1000', '1000-1500', 'more then 1500'];
        }
    }
}

module.exports =  { resHelper, searchHelper, replyHelper };