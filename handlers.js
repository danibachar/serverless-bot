/**
 * Created by danielbachar on 26/08/2017.
 */
//General
const request = require('request');
//Helpers
const res_helper = require("./utils").resHelper;
const searchHelper = require("./utils").searchHelper;
const replyHelper = require("./utils").replyHelper;

//Consts
const url = 'https://dane-search-engine.herokuapp.com/api/search/category/';
const url_test = "https://41ee807e.ngrok.io/api/search/category/"

//Base boot web hook, from here we will nevigate to other lambda functions for digest conversation
module.exports.webhook = (event, context, callback) => {
 //TODO - support different platform
};

module.exports.search_screen_refiner = (event, context, callback) => {

    // console.log('event = ', event);
    console.log('screen size refiner - curr customPayload - ', event.customPayload);
    const category = event.customPayload.category;
    if (!event.reply) {
        //Error handling - back to beginig
        callback(null, res_helper.error_res());
        console.log('event reply is missing');
        return;
    }

    var responseJSON = {
        "response": 'Do you prefer certain screen size?, for specific size just write it down' + replyHelper.replyScreenSizeExample(category), // what the bot will respond with
        "quickReplies": replyHelper.replyScreenSize(), // OPTIONAL: a JSON string array containing suggested/quick replies to display to the user .. i.e., ["Hello","World"]
        "customPayload": event.customPayload
    }
    const res = res_helper.merge_res(responseJSON);

    // callback to return data to Motion AI (must exist, or bot will not work)
    callback(null, res);
};

module.exports.search_price_refiner = (event, context, callback) => {

    // console.log('event = ', event);
    console.log('price refiner - curr customPayload - ', event.customPayload);
    const category = event.customPayload.category;
    if (!event.reply) {
        //Error handling - back to beginig
        callback(null, res_helper.error_res());
        console.log('event reply is missing');
        return;
    }

    var responseJSON = {
        "response": 'hmmm...are you on a budget?', // what the bot will respond with
        "quickReplies": replyHelper.replyPrice(category), // OPTIONAL: a JSON string array containing suggested/quick replies to display to the user .. i.e., ["Hello","World"]
        "customPayload": event.customPayload
    }
    res = res_helper.merge_res(responseJSON);

    // callback to return data to Motion AI (must exist, or bot will not work)
    callback(null, res);
};

module.exports.search = (event, context, callback) => {

    if (!event.reply) {
        //Error handling - back to beginig
        // responseJSON.nextModule = 1018402
        // callback(null, responseJSON);
        console.log('event reply is missing');
        return;
    }

    const userReply = event.reply.toLowerCase();
    searchHelper.getSearchParamsByResponse(userReply, (searchParams, category)=> {
        const options = {
            url: url+category,
            method: "POST",
            json: true,   // <--Very important!!!
            body: {features : search_params}
        };
        request(options, function(err, res, body){
            if (err || !body || body.length === 0) {
                console.log('err - ', err);
            }
            //Preparing customPayload for ext stages
            var customPaylod = {
                "search_params":searchParams
            };
            // this is the object we will return to Motion AI in the callback
            var responseJSON = {
                "response": 'Ahh cool, let me see what I can find for you...', // what the bot will respond with
                "quickReplies": ['Refine?', 'New Search?'], // OPTIONAL: a JSON string array containing suggested/quick replies to display to the user .. i.e., ["Hello","World"]
                "cards":body.slice(0,Math.min(3,body.length)), // OPTIONAL: a cards JSON object to display a carousel to the user (see docs)
                "customPayload": JSON.stringify(customPaylod)
            }
            res = res_helper.merge_res(responseJSON);
            // callback to return data to Motion AI (must exist, or bot will not work)
            callback(null, res);
        });
    });
};