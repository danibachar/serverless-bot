"use strict";

const Wit        = require("node-wit").Wit;
const conf   = require("../../config/conf").get("wit");
const async      = require("async");

const wit = new Wit(conf, {
    say(sessionId, context, message, callback) {
        console.log(message);
        callback();
    },
    merge(sessionId, context, entities, message, callback) {
        callback(context);
    },
    error(sessionId, context, error) {
        log.error(error.message);
    }
});

function recognize(args, callback) {
    async.waterfall([
        async.apply(_witExtraction, args)
    ].concat(extractors), (err, response) => {
        callback({
            score: 0.1,
            resumed: 0,
            promptType: args.promptType,
            response: response
        });
    });
}

function _witExtraction(args, callback) {
    let message = args.utterance;
    if (message && typeof message === "string") {
        message = message.trim();
        wit.message(message.substring(0, 256) /*wit char limit*/, {})
            .then((response) => {
            let entities = {};
        let intent;
        if (!response) {
            console.log("Got empty response from Wit");
        } else {
            entities = _extractEntities(response && response.entities);
            intent = _extractIntent(entities);
        }

        callback(null, {message: message, attachments: args.attachments, entities: entities, intent: intent});
    })
    .catch(error => callback(err));
    } else {
        callback(null, {message: message, attachments: args.attachments, entities: {}});
    }
}

function _extractEntities(entities) {
    let context = {};
    if (entities) {
        for (let entity in entities) {
            if (entities.hasOwnProperty(entity)) {
                context[entity] = entities[entity][0].value;
            }
        }
    }
    return context;
}

function _extractIntent(entities) {
    let intent;
    if (entities.intent) {
        intent = entities.intent;
    } else if (entities.stop) {
        intent = "stop";
    }
    return intent;
}

module.exports = {
    recognize: recognize
};
/**
 * Created by danielbachar on 26/08/2017.
 */
